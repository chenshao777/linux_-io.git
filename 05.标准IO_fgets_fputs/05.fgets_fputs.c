#include <stdio.h>

/*
    功能: 测试使用fputs将内容写入到文件中
          使用fgets将文件中的内容读出
*/
int main(int argc, char *argv[])
{
    FILE *fd;
    char send_buff[] = "hello linux";
    char recv_buff[128] = {0};

    /* 以读写的方式打开一个文件,并且会清空文件 */
    fd = fopen(argv[1], "w+");
    if(fd == NULL){
        printf("打开文件失败!\n");
        return 0;
    }
    
    /* 
        写入数据到文件
        过程: 将用户缓冲区数据写入库缓存
              当遇到\n或者写满了才会将库缓存写入内核缓存
        PS:这里写入的数据中没有\n,但是由于最后调用了fclose,
            关闭文件之前会强行刷新缓冲区的数据,所以会将库缓冲数据写入内核缓冲
    */
    fputs(send_buff, fd);
    fflush(fd);  //强制将缓存数据写入内核缓存

    /*
        读取文件数据
        注意:需要设置文件读写指针
    */
    fseek(fd, 0, SEEK_SET);
    fgets(recv_buff, 128, fd);

    printf("读取到文件数据: %s\n", recv_buff);

    while(1);

    fclose(fd);

    return 0;
}
