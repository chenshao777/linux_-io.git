#include <stdio.h>

int main(int argc, char *argv[])
{
    int i = 10;
    char buff[100]={0};
    //printf只能输出到屏幕上
    printf("i am printf %d\n", i );

    //fprintf可以选择输出流,而且可以格式化
    fprintf(stdout, "i am fprintf %d\n", i+1);

    //sprintf格式化输出到字符串中
    sprintf(buff , "i am sprintf %d\n", i+2);
    printf("%s",buff);

    return 0;
}

