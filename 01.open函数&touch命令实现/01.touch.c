#include <stdio.h>
// #include <sys/types.h>
// #include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

/*
    功能:新建一个文件
    测试命令: ./a.out a.txt
*/
int main(int argc, char *argv[])
{
    /*使用open函数代替touch新建文件*/
    int fd = open(argv[1], O_CREAT | O_RDWR, 0777);
    if(fd < 0){
        printf("create a file failed , fd = %d\n",fd);
    }else{
        printf("create a file success , fd = %d\n",fd);
    }
    /*关闭文件描述符*/
    close(fd);
    return 0;
}
