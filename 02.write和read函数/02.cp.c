#include <unistd.h>
#include <stdio.h>
#include <fcntl.h>
#include <string.h>

/*
    功能:模拟cp命令,拷贝文件内容到另一个文件
    测试命令: ./a.out a.txt b.txt
*/
int main(int argc, char *argv[])
{
    /* 文件描述符 */
    int wr_fd, re_fd;      
    /* 读取文件成功字节数 */
    int re_count;
    /* 读取缓冲区 */
    char read_buff[128];

    /* 判断输入参数个数是否正确 */
    if(argc < 3){
        printf("输入的参数不够\n");
        return 0;
    }

    /* 以只读方式打开源文件 */
    re_fd = open(argv[1], O_RDONLY);
    if(re_fd > 0){
        printf("源文件打开成功, re_fd = %d\n",re_fd);
    }else{
        printf("源文件打开失败, re_fd = %d\n",re_fd);
        return 0;
    }

    /* 以只写方式打开目标文件,并且清空文件 */
    wr_fd = open(argv[2], O_TRUNC | O_WRONLY);
    if(wr_fd > 0){
        printf("目标文件打开成功, wr_fd = %d\n",wr_fd);
    }else{
        printf("目标文件打开失败, wr_fd = %d\n",wr_fd);
        /* 如果打开失败,则新建文件 */
        wr_fd = open(argv[2], O_CREAT | O_WRONLY, 0777);
    }

    for(;;){
        /* 循环读取文件 */
        re_count = read(re_fd, read_buff, sizeof(read_buff));
        /* 如果读取数据不够read_buff的大小,则退出 */
        if(re_count < sizeof(read_buff)){
            break;
        }else{
            /* 读写指针位置定位到文件最后 */
            lseek(wr_fd, 0, SEEK_END);
            /* 数据写入目标文件 */
            write(wr_fd, read_buff, re_count);
            /* 清空读取缓冲区 */
            memset(read_buff, 0, sizeof(read_buff));
        }
    }
    /* 读写指针位置定位到文件最后 */
    lseek(wr_fd, 0, SEEK_END);
    /* 数据写入目标文件 */
    write(wr_fd, read_buff, re_count);
    printf("文件拷贝成功!\n");
    close(re_fd);
    close(wr_fd);
    return 0;
}
