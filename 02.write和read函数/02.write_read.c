#include <unistd.h>
#include <stdio.h>
#include <fcntl.h>
#include <string.h>

/*
    功能:使用write函数向文件a.txt中写入数据，使用read函数读取出数据并打印显示
    测试命令: ./a.out a.txt nihao
*/

int main(int argc, char *argv[])
{
    int fd;
    int wr_res, re_res;
    char wr_buf[100];
    char re_buf[100];
    /*通过main参数接收要写入的内容*/
    sprintf(wr_buf, "%s", argv[2]);

    /*以清空全部内容和读写方式打开文件*/
    fd = open(argv[1], O_TRUNC | O_RDWR);
    if(fd < 0){
        printf("open file failed , fd = %d\n",fd);
        return 0;
    }else{
        printf("open file success , fd = %d\n",fd);
    }

    /*向文件中写入数据*/
    wr_res = write(fd, wr_buf, strlen(wr_buf));
    /*write函数的返回值是写入成功的字节数*/
    printf("write success bytes = %d\n",wr_res);

    /*调整文件定位指针位置, 相对文件开头位置偏移0个位置*/
    lseek(fd, 0, SEEK_SET);

    /*读取文件内容*/
    re_res = read(fd, re_buf, sizeof(re_buf));
    /*read函数的返回值是读取成功的字节数*/
    printf("read success byte = %d\n",re_res);
    printf("read data = %s\n", re_buf);

    close(fd);

    return 0;
}

