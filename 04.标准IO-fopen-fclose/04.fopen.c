#include <stdio.h>

/*
    功能:测试标准IO中 fopen 函数的几种创建文件方式
*/
int main(int argc, char * argv[])
{
    FILE *fd;   //文件流

    /*
        打开文件
        1.使用 w 模式打开,如果文件不存在则创建文件
                        如果文件存在则会删除文件内容
        2.使用 a 模式打开,如果文件不存在则创建文件
                        如果文件存在则会追加
        3.+ 模式表示读写模式
        4.创建的文件默认权限是0666,还要减去umask
    */

    /* 使用 w 模式打开,会清空文件 */
    // fd = fopen(argv[1], "w");
    /* 使用 a 模式打开, 追加形式,不会清空文件 */
    fd = fopen(argv[1], "a");

    if(fd == NULL){
        printf("打开文件失败!\n");
    }else{
        printf("打开文件成功!\n");
    }
    fclose(fd);

    return 0;
}

