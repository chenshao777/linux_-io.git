#include <stdio.h>
#include <unistd.h>

/*
    功能:测试文件IO和标准IO的区别
        文件IO直接将用户缓存写入内核缓存
        标准IO在库缓存中遇到\n或者存满溢出时才会将内容写入内核缓存(打印在终端上)
    测试命令: ./a.out
*/
int main(int argc, char *argv[])
{
    /*
      文件IO会直接将用户缓存写入内核缓存
    */
    write(1, "hello2", 6);

    /*不会输出,因为printf是标准IO,
      标准IO拥有库缓存
      要遇到\n或者将库缓存填满(1024字节)时
      才会将库缓存写入内核缓存*/
    //测试库缓存大小，一共传入128*8=1024个字节
    for(int i = 0; i< 128;i++)
        printf("hello11 ");  //传入库缓存8个字节
    printf("a");  //上面传了1024个,已经把库缓存填满,再发送一个字节才能把数据发送到内核缓存
    while(1);
    return 0;
}

