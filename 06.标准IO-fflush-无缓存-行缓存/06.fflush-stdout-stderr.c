#include <stdio.h>

/*
    功能: 测试fflush强制刷新缓存函数  22行
         测试stdout流是行缓存   33行
         测试stderr流是无缓存   34行
*/
int main(int argc, char *argv[])
{
    FILE *fd;
    char send_buff[] = "hello linux";
    char recv_buff[128] = {0};

    /* 以读写的方式打开一个文件,并且会清空文件 */
    fd = fopen(argv[1], "w+");
    if(fd == NULL){
        printf("打开文件失败!\n");
        return 0;
    }

    fputs(send_buff, fd);
    fflush(fd);  //强制将缓存数据写入内核缓存

    /*
        读取文件数据
        注意:需要设置文件读写指针
    */
    fseek(fd, 0, SEEK_SET);
    fgets(recv_buff, 128, fd);

    printf("读取到文件数据: %s\n", recv_buff);

    fputs("I am stdout", stdout);  //stdout行缓存,不会输出
    fputs("I am stderr", stderr);  //stderr无缓存,直接写入到内核输出

    while(1);   //死循环,不让fclose运行

    fclose(fd);

    return 0;
}
