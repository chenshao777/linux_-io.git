#include <stdio.h>

/*
    功能: 测试 fgetc fputc  不是行缓存,但有缓存
*/
int main(int argc, char *argv[])
{
    FILE *fd;
    int res;
    fd = fopen("./a.txt", "w+");

    //向a.txt写入一个字符
    fputc('a', fd);
    //测试fputc是否是行缓存(不是)
    // fputc('\n',fd);
    // fflush(fd);
    // while(1);

    //将读写指针恢复到开头
    rewind(fd);
    //从a.txt读取一个字符
    res = fgetc(fd);
    printf("读取到字符 = %c\n", res);

    fclose(fd);
    return 0;
}

