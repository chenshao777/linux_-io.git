#include <stdio.h>
#include <string.h>
#include <unistd.h>

/*
    功能:测试gets fgets puts fputs  都是行缓存
*/

int main(int argc, char *argv[])
{
    char buff[128] = {0};
    //只能从键盘输入获得,不将新行存入缓冲
    //没有指定缓存的长度,可能造成溢出,不安全
    gets(buff); 
    printf("buff1 = %s\n",buff);

    memset(buff, 0, sizeof(buff));
    //可以指定获取流,将新行存入缓存中
    fgets(buff, 128, stdin);
    printf("buff2 = %s\n",buff);

    //相当于printf("hello\n"),自动输出新行
    puts("hello");

    //可以选择输出的流,且不会自动包含新行
    FILE *fd;
    fd = fopen("./b.txt", "a+");
    fputs("hello linux",fd);  //行缓存
    fflush(fd);   //强制刷新缓存
    while(1);
    fclose(fd);
    return 0;
}
